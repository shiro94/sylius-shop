<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'TYPO3 Sylius Shop Extension',
    'description' => 'Create a shop using mostly sylius components',
    'category' => 'fe',
    'state' => 'alpha',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'author' => 'Christian Gebing',
    'author_email' => 'c.gebing@neusta.de',
    'author_company' => '',
    'version' => '0.0.1',
    'constraints' => [
        'depends' => [
            'typo3' => '9.3.0',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
